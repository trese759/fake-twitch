/* eslint-disable @typescript-eslint/no-explicit-any */
import { get, Domains } from '../api';
import { StreamCreateResponse } from '@fake-twitch/interfaces';

/**
 * @brief Get all streams request
 */
export const getStreams = async (): Promise<StreamCreateResponse[]> => {
	const response = await get('streams', Domains.STREAMS);
	return response.json();
}