/* eslint-disable @typescript-eslint/no-explicit-any */
import { remove, Domains } from '../api';

/**
 * @brief Delete specified stream
 * @param id Id of stream to delete
 */
export const deleteStream = async (id: number): Promise<void> => {
	const response = await remove(`streams`, id, Domains.STREAMS);
	return response.json();
}