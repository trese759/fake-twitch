/* eslint-disable @typescript-eslint/no-explicit-any */
import { put, Domains } from '../api';
import { StreamCreateResponse, StreamCreateRequest } from '@fake-twitch/interfaces';

/**
 * @brief Update specified stream
 * @param payload Updated data
 */
export const updateStream = async (payload: StreamCreateRequest): Promise<StreamCreateResponse> => {
	const response = await put('streams', payload.id, payload, Domains.STREAMS);
	return response.json();
}