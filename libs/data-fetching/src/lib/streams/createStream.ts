/* eslint-disable @typescript-eslint/no-explicit-any */
import { post, Domains } from '../api';
import { StreamCreateRequest, StreamCreateResponse } from '@fake-twitch/interfaces';

/**
 * Create stream post request
 * @param payload Request data
 */
export const createStream = async (payload: StreamCreateRequest): Promise<StreamCreateResponse> => {
	const response = await post('streams', payload, Domains.STREAMS);
	return response.json();
}