/* eslint-disable @typescript-eslint/no-explicit-any */
import { get, Domains } from '../api';
import { StreamCreateResponse } from '@fake-twitch/interfaces';

/**
 * @brief Get specified stream
 * @param id Id of stream to get
 */
export const getStream = async (id: number): Promise<StreamCreateResponse> => {
	const response = await get(`streams/${id}`, Domains.STREAMS);
	return response.json();
}