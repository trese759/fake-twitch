/* eslint-disable @typescript-eslint/no-explicit-any */
import wretch, { ResponseChain } from 'wretch';

export enum Domains {
	STRAPI = 'http://localhost:1337/',
	STREAMS = 'http://localhost:3001/'
}

/**
 * @brief Base api call
 * @param endpoint Endpoint to call
 */
const api = (endpoint: string, domain = Domains.STRAPI) => {
	return wretch()
		.headers({
			'Content-Type': 'application/json',
		})
		.url(domain + endpoint);
};

/**
 * @brief Perform a get request
 * @param endpoint Endpoint to call
 * @param queryParams Parameters to add at request
 * @param domain Domain to fetch
 */
export const get = (
	endpoint: string,
	domain = Domains.STRAPI
): ResponseChain & Promise<any> => api(endpoint, domain).get();

/**
 * @brief Perform a post request
 * @param endpoint Endpoint to call
 * @param data Object to save
 * @param domain Domain to fetch
 */
export const post = (
	endpoint: string,
	data = {},
	domain = Domains.STRAPI
): ResponseChain & Promise<any> => api(endpoint, domain).json(data).post();

/**
 * @brief Perform a put request
 * @param endpoint Endpoint to call
 * @param id Id of record to update
 * @param data Updated object
 * @param domain Domain to fetch
 */
export const put = (
	endpoint: string,
	id: number,
	data = {},
	domain = Domains.STRAPI
): ResponseChain & Promise<any> => api(`${endpoint}/${id}`, domain).json(data).put();

/**
 * @brief Perform a delete request
 * @param endpoint Endpoint to call
 * @param id Id of record to delete
 * @param domain Domain to fetch
 */
export const remove = (
	endpoint: string,
	id: number,
	domain = Domains.STRAPI
): ResponseChain & Promise<any> => api(`${endpoint}/${id}`, domain).delete();
