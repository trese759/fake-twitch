import { post } from '../../api';
import { LoginRequest, AuthenticatedResponse } from '@fake-twitch/interfaces';

/**
 * @brief User authentication request (Login)
 * @param request Request data
 */
const authenticateUser = async (
	request: LoginRequest
): Promise<AuthenticatedResponse> => {
	const response = await post('auth/local', request);
	return response.json();
};

export default { authenticateUser };
