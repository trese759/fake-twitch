import { post } from '../../api';
import {
	RegisterRequest,
	AuthenticatedResponse,
} from '@fake-twitch/interfaces';

/**
 * @brief Register new user to application
 * @param request Request data
 */
const registerUser = async (
	request: RegisterRequest
): Promise<AuthenticatedResponse> => {
	const response = await post('auth/local/register', request);
	return response.json();
};

export default registerUser;
