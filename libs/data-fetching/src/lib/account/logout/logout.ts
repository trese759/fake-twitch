/** @brief Logout user from application */
const logout = (): Promise<void> =>
	new Promise<void>((resolve) => resolve(sessionStorage.clear()));

export default logout;
