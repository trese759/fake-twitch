interface StreamCreateResponse {
    id: number;
    title: string;
    description: string;
    userId: number;
}

export default StreamCreateResponse;