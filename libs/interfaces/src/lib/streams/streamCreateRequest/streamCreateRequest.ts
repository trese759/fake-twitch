interface StreamCreateRequest {
    id?: number;
    title: string;
    description: string;
    userId: string;
};

export default StreamCreateRequest;