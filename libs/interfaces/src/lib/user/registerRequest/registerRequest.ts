interface RegisterRequest {
	username: string;
	email: string;
	password: string;
	firstName: string;
	secondName: string;
}

export default RegisterRequest;
