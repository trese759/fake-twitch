interface AuthenticatedResponse {
	jwt: string;
	user: {
		id: number;
		blocked: boolean;
		confirmed: boolean;
		email: string;
		username: string;
		firstName: string;
		secondName: string;
		provider: string;
		created_at: string;
		updated_at: string;
		role: {
			id: number;
			name: string;
			type: string;
			description: string;
		};
	};
}

export default AuthenticatedResponse;
