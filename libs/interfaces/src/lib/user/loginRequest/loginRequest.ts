interface LoginRequest {
	identifier: string;
	password: string;
}

export default LoginRequest;
