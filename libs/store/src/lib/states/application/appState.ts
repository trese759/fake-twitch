import { UserState } from '../user';
import { StreamsState } from '../streams';

interface AppState {
	user: UserState;
	streams: StreamsState;
}

export default AppState;
