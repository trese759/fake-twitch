import { BaseState } from '../base';

interface User {
	id: number;
	email: string;
	username: string;
	firstName: string;
	secondName: string;
	role: {
		id: number;
		name: string;
	};
}

interface UserState {
	readonly data: User;
	readonly base: BaseState;
}

export { UserState, User };
