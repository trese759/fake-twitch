import { UserState } from '..';
import { BaseState } from '../base';
import { AppState } from '../application';
import { StreamsState } from '../streams/streams';

export const initialBaseState: BaseState = {
	error: '',
	isFetched: false,
	isFetching: false,
	rollback: [],
};

export const initialUserState: UserState = {
	data: {
		email: '',
		firstName: '',
		id: 0,
		secondName: '',
		username: '',
		role: {
			id: 0,
			name: '',
		},
	},
	base: initialBaseState,
};

export const initialStreamsState: StreamsState = {
	data: {},
	base: initialBaseState
};

export const initialAppState: AppState = {
	user: initialUserState,
	streams: initialStreamsState
};
