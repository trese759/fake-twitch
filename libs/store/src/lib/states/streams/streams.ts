import { StreamCreateResponse } from '@fake-twitch/interfaces';
import { BaseState } from '../base';

interface BaseStream {
    isFetching: boolean;
    isFetched: boolean;
    isCreated: boolean;
    isEdited: boolean;
    isDeleted: boolean;
    error?: string;
}

export type StreamInState = StreamCreateResponse & BaseStream;
export type Stream = { [key: string]: StreamInState };

export type StreamsState = {
    readonly data: Stream;
    readonly base: BaseState;
}