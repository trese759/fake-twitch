interface BaseState {
	readonly isFetching: boolean;
	readonly isFetched: boolean;
	readonly rollback: unknown;
	readonly error?: string;
}

export default BaseState;
