import { createSelector } from 'reselect';
import { useDispatch, useSelector } from 'react-redux';

import { AppState, User } from '../../states';
import { LoginRequest, RegisterRequest } from '@fake-twitch/interfaces';
import { fetchUser, fetchUserLogout, fetchUserCreate } from '../../actions';

const getUser = (state: AppState) => state.user.data;
const getIsFetching = (state: AppState) => state.user.base.isFetching;
const getIsFetched = (state: AppState) => state.user.base.isFetched;
const getError = (state: AppState) => state.user.base.error;

const selectorGetUser = createSelector([getUser], (user: User) => user);

const selectorIsFetching = createSelector(
	[getIsFetching],
	(isFetching: boolean) => isFetching
);

const selectorIsFetched = createSelector(
	[getIsFetched],
	(isFetched: boolean) => isFetched
);

const selectorError = createSelector([getError], (error: string) => error);

export const useUser = () => {
	const user = useSelector<AppState, User>(selectorGetUser);
	return { user };
};

export const useLogin = (payload: LoginRequest) => {
	const dispatch = useDispatch();

	const user = useSelector<AppState, User>(selectorGetUser);
	const isFetched = useSelector<AppState, boolean>(selectorIsFetched);
	const isFetching = useSelector<AppState, boolean>(selectorIsFetching);
	const error = useSelector<AppState, string>(selectorError);

	const login = () => dispatch(fetchUser.trigger(payload));

	return {
		user,
		error,
		isFetched,
		isFetching,
		login,
	};
};

export const useRegister = (payload: RegisterRequest) => {
	const dispatch = useDispatch();

	const user = useSelector<AppState, User>(selectorGetUser);
	const isFetched = useSelector<AppState, boolean>(selectorIsFetched);
	const isFetching = useSelector<AppState, boolean>(selectorIsFetching);
	const error = useSelector<AppState, string>(selectorError);

	const register = () => dispatch(fetchUserCreate.trigger(payload));

	return {
		user,
		error,
		isFetched,
		isFetching,
		register,
	};
};

export const useLogout = () => {
	const dispatch = useDispatch();
	const logout = () => dispatch(fetchUserLogout.trigger());
	return { logout };
};
