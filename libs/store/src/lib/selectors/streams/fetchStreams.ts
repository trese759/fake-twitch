import { AppState, StreamsState } from "../../states";
import { createSelector } from 'reselect';

import { useSelector, useDispatch } from 'react-redux';
import { fetchStreamCreate, fetchStreams, fetchStream, fetchStreamDelete, fetchStreamUpdate } from '../../actions';
import { useEffect, useCallback } from 'react';
import { StreamCreateRequest } from '@fake-twitch/interfaces';

const getStreams = (state: AppState) => state.streams;
const getIsFetching = (state: AppState) => state.streams.base.isFetching;
const getIsFetched = (state: AppState) => state.streams.base.isFetched;
const getError = (state: AppState) => state.streams.base.error;

const selectorGetStreams = createSelector([getStreams], (streams: StreamsState) => streams);
const selectorIsFetching = createSelector([getIsFetching], (isFetching: boolean) => isFetching);
const selectorIsFetched = createSelector([getIsFetched], (isFetched: boolean) => isFetched);
const selectorError = createSelector([getError], (error: string) => error);

export const useStreams = () => {
	const dispatch = useDispatch();

	const streams = useSelector<AppState, StreamsState>(selectorGetStreams);
	const isFetched = useSelector<AppState, boolean>(selectorIsFetched);
	const isFetching = useSelector<AppState, boolean>(selectorIsFetching);
	const error = useSelector<AppState, string>(selectorError);

	useEffect(() => {
		if (Object.keys(streams.data).length === 0) {
			dispatch(fetchStreams.trigger());
		}
	}, [dispatch, streams])

	const fetchStreamById = useCallback((id: number) => {
		dispatch(fetchStream.trigger(id))
	}, [dispatch]);

	const createStream = useCallback((payload: StreamCreateRequest) => {
		dispatch(fetchStreamCreate.trigger(payload));
	}, [dispatch]);

	const deleteStream = useCallback((id: number) => {
		dispatch(fetchStreamDelete.trigger(id));
	}, [dispatch]);

	const editStream = useCallback((payload: StreamCreateRequest) => {
		dispatch(fetchStreamUpdate.trigger(payload));
	}, [dispatch]);

	return { 
		streams: streams.data,
		isFetched,
		isFetching,
		error,
		fetchStreamById,
		createStream,
		deleteStream,
		editStream
	};
};