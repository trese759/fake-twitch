/* eslint-disable @typescript-eslint/no-explicit-any */
import { createReducer } from "typesafe-actions";
import { initialStreamsState } from '../../states/initial/initial';
import FetchStreamCreate from '../../types/streams/fetchCreate';
import { StreamsState } from '../../states/streams/streams';
import { Reducer } from 'redux';
import { FetchStream, FetchStreams, FetchStreamDelete, FetchStreamUpdate } from '../../types';

const streamsReducer: Reducer<StreamsState> = createReducer(initialStreamsState)
	.handleAction(
		[
			FetchStream.FETCH_STREAM,
			FetchStreams.FETCH_STREAMS,
			FetchStreamCreate.FETCH_STREAM_CREATE,
			FetchStreamDelete.FETCH_STREAM_DELETE,
			FetchStreamUpdate.FETCH_STREAM_UPDATE
		],
		(state: StreamsState): StreamsState => state
	)
	.handleAction(
		[
			FetchStream.FETCH_STREAM_REQUEST,
			FetchStreams.FETCH_STREAMS_REQUEST,
			FetchStreamCreate.FETCH_STREAM_CREATE_REQUEST,
			FetchStreamDelete.FETCH_STREAM_DELETE_REQUEST,
			FetchStreamUpdate.FETCH_STREAM_UPDATE_REQUEST
		],
		(state: StreamsState): StreamsState => state
	)
	.handleAction(
		FetchStreams.FETCH_STREAMS_SUCCESS,
		(state: StreamsState, action: any): StreamsState => {
			return {
				...state,
				data: {...action.payload.reduce((acc, curr) => {
					return {...acc, [curr.id]: {
						...curr,
						isFetched: true,
						isFetching: false
					}}}, {})
				},
				base: {
					isFetched: true,
					isFetching: false,
					rollback: { ...state.data }
				}
			}
		}
	)
	.handleAction(FetchStream.FETCH_STREAM_SUCCESS,
		(state: StreamsState, action: any): StreamsState => {
			return { 
				...state, 
				data: {
					...state.data,
					[action.payload.id]: {
						...action.payload,
						isFetched: true,
						isFetching: false,
					}
				},
				base: {
					isFetched: true,
					isFetching: false,
					rollback: {...state.data}
				}
			}
		}
	)
	.handleAction(FetchStreamUpdate.FETCH_STREAM_UPDATE_SUCCESS,
		(state: StreamsState, action: any): StreamsState => {
			return { 
				...state, 
				data: {
					...state.data,
					[action.payload.id]: {
						...action.payload,
						isFetching: false,
						isEdited: true
					}
				},
				base: {
					isFetched: true,
					isFetching: false,
					rollback: {...state.data}
				}
			}
		}
	)
	.handleAction(FetchStreamCreate.FETCH_STREAM_CREATE_SUCCESS,
		(state: StreamsState, action: any): StreamsState => {
			return { 
				...state, 
				data: {
					...state.data,
					[action.payload.id]: {
						...action.payload,
						isFetching: false,
						isCreated: true
					}
				},
				base: {
					isFetched: true,
					isFetching: false,
					rollback: {...state.data}
				}
			}
		}
	)
	.handleAction(
		FetchStreamDelete.FETCH_STREAM_DELETE_SUCCESS,
		(state: StreamsState, action: any): StreamsState => {
			const clone = { ...state };
			delete clone.data[action.payload];
			return {
				...state,
				data: clone.data,
				base: {
					isFetched: true,
					isFetching: false,
					rollback: {...state}
				}
			}
		}
	)
	.handleAction(
		[
			FetchStream.FETCH_STREAM_ERROR,
			FetchStreams.FETCH_STREAMS_ERROR,
			FetchStreamCreate.FETCH_STREAM_CREATE_ERROR,
			FetchStreamDelete.FETCH_STREAM_DELETE_ERROR,
			FetchStreamUpdate.FETCH_STREAM_UPDATE_ERROR
		],
		(state: StreamsState, action: any): StreamsState => {
			return { 
				...state, 
				data: {...state.data},
				base: {
					isFetched: false,
					isFetching: false,
					error: action.payload,
					rollback: {...state.data}
				}
			}
		}
	);

export default streamsReducer;