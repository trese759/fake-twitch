/* eslint-disable @typescript-eslint/no-explicit-any */
import { UserState, initialUserState } from '../../states';
import { Reducer } from 'redux';
import { createReducer } from 'typesafe-actions';
import FetchUser from '../../types/user/fetchUser';
import FetchUserLogout from '../../types/user/fetchLogout';

import { AuthenticatedResponse } from '@fake-twitch/interfaces';
import FetchUserCreate from '../../types/user/fetchCreate';

const userReducer: Reducer<UserState> = createReducer(initialUserState)
	.handleAction(
		FetchUser.FETCH_USER_REQUEST,
		(state: UserState): UserState => state
	)
	.handleAction(
		FetchUserCreate.FETCH_USER_CREATE_REQUEST,
		(state: UserState): UserState => state
	)
	.handleAction(
		FetchUser.FETCH_USER_SUCCESS,
		(state: UserState, action: any): UserState => {
			const { user } = action.payload as AuthenticatedResponse;
			return {
				...state,
				data: {
					email: user.email,
					firstName: user.firstName,
					id: user.id,
					secondName: user.secondName,
					username: user.username,
					role: {
						id: user.role.id,
						name: user.role.name,
					},
				},
				base: {
					error: '',
					isFetched: true,
					isFetching: false,
					rollback: state.data,
				},
			};
		}
	)
	.handleAction(
		FetchUserCreate.FETCH_USER_CREATE_SUCCESS,
		(state: UserState, action: any): UserState => {
			const { user } = action.payload as AuthenticatedResponse;
			return {
				...state,
				data: {
					email: user.email,
					firstName: user.firstName,
					id: user.id,
					secondName: user.secondName,
					username: user.username,
					role: {
						id: user.role.id,
						name: user.role.name,
					},
				},
				base: {
					error: '',
					isFetched: true,
					isFetching: false,
					rollback: state.data,
				},
			};
		}
	)
	.handleAction(
		FetchUser.FETCH_USER_ERROR,
		(state: UserState, action: any): UserState => {
			return {
				...state,
				data: {
					email: '',
					firstName: '',
					id: 0,
					secondName: '',
					username: '',
					role: {
						id: 0,
						name: '',
					},
				},
				base: {
					error: action.payload.text,
					isFetched: true,
					isFetching: false,
					rollback: null,
				},
			};
		}
	)
	.handleAction(
		FetchUserCreate.FETCH_USER_CREATE_ERROR,
		(state: UserState, action: any): UserState => {
			return {
				...state,
				data: {
					email: '',
					firstName: '',
					id: 0,
					secondName: '',
					username: '',
					role: {
						id: 0,
						name: '',
					},
				},
				base: {
					error: action.payload.text,
					isFetched: true,
					isFetching: false,
					rollback: null,
				},
			};
		}
	)
	.handleAction(
		FetchUserLogout.FETCH_USER_LOGOUT,
		(state: UserState): UserState => state
	)
	.handleAction(
		FetchUserLogout.FETCH_USER_LOGOUT_SUCCESS,
		(): UserState => initialUserState
	)
	.handleAction(
		FetchUserLogout.FETCH_USER_LOGOUT_ERROR,
		(): UserState => initialUserState
	);

export default userReducer;
