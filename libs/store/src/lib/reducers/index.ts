import { combineReducers } from 'redux';
import { userReducer } from './user';
import { streamsReducer } from './streams';

const rootReducer = combineReducers({
	user: userReducer,
	streams: streamsReducer
});

export default rootReducer;
