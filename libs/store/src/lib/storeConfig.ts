import createSagaMiddleware from 'redux-saga';
import { Store, createStore, applyMiddleware } from 'redux';
import { composeWithDevTools } from 'redux-devtools-extension';
import { AppState } from './states';
import rootReducer from './reducers';
import { rootSaga } from './sagas';

export const storeConfig = (initialState: AppState): Store<AppState> => {
	const sagaMiddleware = createSagaMiddleware();
	const store = createStore(
		rootReducer,
		initialState,
		composeWithDevTools(applyMiddleware(sagaMiddleware))
	);

	sagaMiddleware.run(rootSaga);
	return store;
};
