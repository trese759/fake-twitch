import { call, put, all, takeEvery } from 'redux-saga/effects';

import { fetchUser } from '../../actions';
import { authentication } from '@fake-twitch/data-fetching';
import { AuthenticatedResponse } from '@fake-twitch/interfaces';

function* handleFetch(action: ReturnType<typeof fetchUser.trigger>) {
	yield call(fetchUser.request, action.payload);

	try {
		const response: AuthenticatedResponse = yield call(
			authentication.authenticateUser,
			action.payload
		);

		sessionStorage.setItem('token', response.jwt);
		sessionStorage.setItem('userId', response.user.id.toString());
		yield put(fetchUser.success(response));
	} catch (error) {
		yield put(fetchUser.failure(error));
	}
}

export function* fetchUserSaga() {
	yield all([takeEvery(fetchUser.trigger, handleFetch)]);
}
