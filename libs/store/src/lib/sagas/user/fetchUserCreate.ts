import { call, put, all, takeEvery } from 'redux-saga/effects';

import { fetchUserCreate } from '../../actions';
import { registerUser } from '@fake-twitch/data-fetching';
import { AuthenticatedResponse } from '@fake-twitch/interfaces';

function* handleFetch(action: ReturnType<typeof fetchUserCreate.trigger>) {
	yield call(fetchUserCreate.request, action.payload);

	try {
		const response: AuthenticatedResponse = yield call(
			registerUser,
			action.payload
		);

		sessionStorage.setItem('token', response.jwt);
		sessionStorage.setItem('userId', response.user.id.toString());
		yield put(fetchUserCreate.success(response));
	} catch (error) {
		yield put(fetchUserCreate.failure(error));
	}
}

export function* fetchUserCreateSaga() {
	yield all([takeEvery(fetchUserCreate.trigger, handleFetch)]);
}
