import { call, put, all, takeEvery } from 'redux-saga/effects';

import { fetchUserLogout } from '../../actions';
import { logout } from '@fake-twitch/data-fetching';

function* handleFetch() {
	yield call(fetchUserLogout.request);

	try {
		const response = yield call(logout);
		sessionStorage.clear();
		yield put(fetchUserLogout.success(response));
	} catch (error) {
		yield put(fetchUserLogout.failure(error));
	}
}

export function* fetchUserLogoutSaga() {
	yield all([takeEvery(fetchUserLogout.trigger, handleFetch)]);
}
