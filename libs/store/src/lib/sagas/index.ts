import { all, fork } from 'redux-saga/effects';
import { fetchUserSaga } from './user/fetchUser';
import { fetchUserLogoutSaga } from './user/fetchUserLogout';
import { fetchUserCreateSaga } from './user/fetchUserCreate';
import { fetchStreamCreateSaga } from './streams/fetchStreamCreate';
import { fetchStreamSaga } from './streams/fetchStream';
import { fetchStreamsSaga } from './streams/fetchStreams';
import { fetchStreamDeleteSaga } from './streams/fetchStreamDelete';
import { fetchStreamUpdateSaga } from './streams/fetchStreamUpdate';

export function* rootSaga() {
	yield all([
		fork(fetchUserSaga),
		fork(fetchUserLogoutSaga),
		fork(fetchUserCreateSaga),
		fork(fetchStreamSaga),
		fork(fetchStreamsSaga),
		fork(fetchStreamCreateSaga),
		fork(fetchStreamDeleteSaga),
		fork(fetchStreamUpdateSaga)
	]);
}
