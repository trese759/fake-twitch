import { call, put, all, takeEvery } from 'redux-saga/effects';

import { fetchStreamUpdate } from '../../actions';
import { updateStream } from '@fake-twitch/data-fetching';

function* handleFetch(action: ReturnType<typeof fetchStreamUpdate.trigger>) {
	yield call(fetchStreamUpdate.request, action.payload);

	try {
		const response = yield call(updateStream, action.payload);
		yield put(fetchStreamUpdate.success(response));
	} catch (error) {
		yield put(fetchStreamUpdate.failure(error));
	}
}

export function* fetchStreamUpdateSaga() {
	yield all([takeEvery(fetchStreamUpdate.trigger, handleFetch)]);
}