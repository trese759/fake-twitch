import { call, put, all, takeEvery } from 'redux-saga/effects';

import { fetchStreams } from '../../actions';
import { getStreams } from '@fake-twitch/data-fetching';

function* handleFetch() {
	yield call(fetchStreams.request);

	try {
		const response = yield call(getStreams);
		yield put(fetchStreams.success(response));
	} catch (error) {
		yield put(fetchStreams.failure(error));
	}
}

export function* fetchStreamsSaga() {
	yield all([takeEvery(fetchStreams.trigger, handleFetch)]);
}