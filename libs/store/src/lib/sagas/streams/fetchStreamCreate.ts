import { call, put, all, takeEvery } from 'redux-saga/effects';

import { fetchStreamCreate } from '../../actions';
import { createStream } from '@fake-twitch/data-fetching';

function* handleFetch(action: ReturnType<typeof fetchStreamCreate.trigger>) {
	yield call(fetchStreamCreate.request, action.payload);

	try {
		const response = yield call(createStream, action.payload);
		yield put(fetchStreamCreate.success(response));
	} catch (error) {
		yield put(fetchStreamCreate.failure(error));
	}
}

export function* fetchStreamCreateSaga() {
	yield all([takeEvery(fetchStreamCreate.trigger, handleFetch)]);
}