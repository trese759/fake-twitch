import { call, put, all, takeEvery } from 'redux-saga/effects';

import { fetchStream } from '../../actions';
import { getStream } from '@fake-twitch/data-fetching';

function* handleFetch(action: ReturnType<typeof fetchStream.trigger>) {
	yield call(fetchStream.request, action.payload);

	try {
		const response = yield call(getStream, action.payload);
		yield put(fetchStream.success(response));
	} catch (error) {
		yield put(fetchStream.failure(error));
	}
}

export function* fetchStreamSaga() {
	yield all([takeEvery(fetchStream.trigger, handleFetch)]);
}