import { call, put, all, takeEvery } from 'redux-saga/effects';

import { fetchStreamDelete } from '../../actions';
import { deleteStream } from '@fake-twitch/data-fetching';

function* handleFetch(action: ReturnType<typeof fetchStreamDelete.trigger>) {
	yield call(fetchStreamDelete.request, action.payload);

	try {
		yield call(deleteStream, action.payload);
		yield put(fetchStreamDelete.success(action.payload));
	} catch (error) {
		yield put(fetchStreamDelete.failure(error));
	}
}

export function* fetchStreamDeleteSaga() {
	yield all([takeEvery(fetchStreamDelete.trigger, handleFetch)]);
}