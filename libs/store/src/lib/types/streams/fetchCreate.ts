enum FetchStreamCreate {
    FETCH_STREAM_CREATE_REQUEST = 'action/FETCH_STREAM_CREATE_REQUEST',
    FETCH_STREAM_CREATE_SUCCESS = 'action/FETCH_STREAM_CREATE_SUCCESS',
    FETCH_STREAM_CREATE_ERROR = 'action/FETCH_STREAM_CREATE_ERROR',
    FETCH_STREAM_CREATE = 'trigger/FETCH_STREAM_CREATE',
};

export default FetchStreamCreate;