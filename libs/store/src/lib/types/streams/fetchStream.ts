enum FetchStream {
    FETCH_STREAM_REQUEST = 'action/FETCH_STREAM_REQUEST',
    FETCH_STREAM_SUCCESS = 'action/FETCH_STREAM_SUCCESS',
    FETCH_STREAM_ERROR = 'action/FETCH_STREAM_ERROR',
    FETCH_STREAM = 'trigger/FETCH_STREAM',
};

export default FetchStream;