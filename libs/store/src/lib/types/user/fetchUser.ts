enum FetchUser {
	FETCH_USER_REQUEST = 'action/FETCH_USER_REQUEST',
	FETCH_USER_SUCCESS = 'action/FETCH_USER_SUCCESS',
	FETCH_USER_ERROR = 'action/FETCH_USER_ERROR',
	FETCH_USER = 'trigger/FETCH_USER'
}

export default FetchUser;
