import { createAsyncAction, createAction } from 'typesafe-actions';
import FetchUser from '../../types/user/fetchUser';
import { LoginRequest, AuthenticatedResponse } from '@fake-twitch/interfaces';

/** @brief Fetch User typesafe actions */
const fetchUser = createAsyncAction(
	FetchUser.FETCH_USER_REQUEST,
	FetchUser.FETCH_USER_SUCCESS,
	FetchUser.FETCH_USER_ERROR
)<LoginRequest, AuthenticatedResponse, {}>();

/** @brief Trigger action for fetch user */
const trigger = createAction(FetchUser.FETCH_USER)<LoginRequest>();

export default { trigger, ...fetchUser };