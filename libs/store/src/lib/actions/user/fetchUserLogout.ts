import { createAsyncAction, createAction } from 'typesafe-actions';
import FetchUserLogout from '../../types/user/fetchLogout';

/** @brief Fetch User creation with typesafe actions */
export const fetchUserLogout = createAsyncAction(
	FetchUserLogout.FETCH_USER_LOGOUT_REQUEST,
	FetchUserLogout.FETCH_USER_LOGOUT_SUCCESS,
	FetchUserLogout.FETCH_USER_LOGOUT_ERROR
)<void, void, void>();

/** @brief Trigger action for fetch user create */
const trigger = createAction(FetchUserLogout.FETCH_USER_LOGOUT)<void>();

export default { trigger, ...fetchUserLogout }