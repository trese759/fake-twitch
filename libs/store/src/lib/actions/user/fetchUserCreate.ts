import { createAsyncAction, createAction } from 'typesafe-actions';
import FetchUserCreate from '../../types/user/fetchCreate';
import {
	RegisterRequest,
	AuthenticatedResponse,
} from '@fake-twitch/interfaces';

/** @brief Fetch User creation with typesafe actions */
const fetchUserCreate = createAsyncAction(
	FetchUserCreate.FETCH_USER_CREATE_REQUEST,
	FetchUserCreate.FETCH_USER_CREATE_SUCCESS,
	FetchUserCreate.FETCH_USER_CREATE_ERROR
)<RegisterRequest, AuthenticatedResponse, {}>();

/** @brief Trigger action for fetch user create */
const trigger = createAction(FetchUserCreate.FETCH_USER_CREATE)<RegisterRequest>();

export default { trigger, ...fetchUserCreate };