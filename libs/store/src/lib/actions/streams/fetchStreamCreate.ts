import { createAsyncAction, createAction } from "typesafe-actions";
import FetchStreamCreate from '../../types/streams/fetchCreate';
import { StreamCreateRequest, StreamCreateResponse } from '@fake-twitch/interfaces';

const fetchStreamCreate = createAsyncAction(
	FetchStreamCreate.FETCH_STREAM_CREATE_REQUEST,
	FetchStreamCreate.FETCH_STREAM_CREATE_SUCCESS,
	FetchStreamCreate.FETCH_STREAM_CREATE_ERROR
)<StreamCreateRequest, StreamCreateResponse, string>();

const trigger = createAction(FetchStreamCreate.FETCH_STREAM_CREATE)<StreamCreateRequest>();

export default { trigger, ...fetchStreamCreate };