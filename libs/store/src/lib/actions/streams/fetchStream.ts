import { createAsyncAction, createAction } from "typesafe-actions";
import { FetchStream } from '../../types/streams';
import { StreamCreateResponse } from '@fake-twitch/interfaces';

const fetchStream = createAsyncAction(
	FetchStream.FETCH_STREAM_REQUEST,
	FetchStream.FETCH_STREAM_SUCCESS,
	FetchStream.FETCH_STREAM_ERROR
)<number, StreamCreateResponse, string>();

const trigger = createAction(FetchStream.FETCH_STREAM)<number>();

export default { trigger, ...fetchStream };