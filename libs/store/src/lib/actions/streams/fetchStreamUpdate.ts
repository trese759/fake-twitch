import { createAsyncAction, createAction } from "typesafe-actions";
import { FetchStreamUpdate } from '../../types/streams';
import { StreamCreateResponse, StreamCreateRequest } from '@fake-twitch/interfaces';

const fetchStreamUpdate = createAsyncAction(
	FetchStreamUpdate.FETCH_STREAM_UPDATE_REQUEST,
	FetchStreamUpdate.FETCH_STREAM_UPDATE_SUCCESS,
	FetchStreamUpdate.FETCH_STREAM_UPDATE_ERROR
)<StreamCreateRequest, StreamCreateResponse, string>();

const trigger = createAction(FetchStreamUpdate.FETCH_STREAM_UPDATE)<StreamCreateRequest>();

export default { trigger, ...fetchStreamUpdate };