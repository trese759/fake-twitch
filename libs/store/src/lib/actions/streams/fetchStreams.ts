import { createAsyncAction, createAction } from "typesafe-actions";
import { FetchStreams } from '../../types/streams';
import { StreamCreateResponse } from '@fake-twitch/interfaces';

const fetchStreams = createAsyncAction(
	FetchStreams.FETCH_STREAMS_REQUEST,
	FetchStreams.FETCH_STREAMS_SUCCESS,
	FetchStreams.FETCH_STREAMS_ERROR
)<void, StreamCreateResponse[], string>();

const trigger = createAction(FetchStreams.FETCH_STREAMS)<void>();

export default { trigger, ...fetchStreams };