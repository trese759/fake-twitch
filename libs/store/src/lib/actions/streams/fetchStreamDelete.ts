import { createAsyncAction, createAction } from "typesafe-actions";
import { FetchStreamDelete } from '../../types/streams';

const fetchStreamDelete = createAsyncAction(
	FetchStreamDelete.FETCH_STREAM_DELETE_REQUEST,
	FetchStreamDelete.FETCH_STREAM_DELETE_SUCCESS,
	FetchStreamDelete.FETCH_STREAM_DELETE_ERROR
)<number, number, string>();

const trigger = createAction(FetchStreamDelete.FETCH_STREAM_DELETE)<number>();

export default { trigger, ...fetchStreamDelete };