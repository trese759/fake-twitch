import React, { memo, useCallback } from 'react';
import { useForm } from 'react-hook-form';

import { IconInputField } from '../../molecules';
import { BaseButton, BaseInputField, LabelError } from '../../atoms';
import { RegisterRequest } from '@fake-twitch/interfaces';

interface Props {
	onSubmit: (data: RegisterRequest) => void;
}

const RegistrationForm: React.FC<Props> = ({ onSubmit }): JSX.Element => {
	const { register, handleSubmit, errors } = useForm({
		mode: 'onChange',
	});
	const onFormSubmit = useCallback((data: RegisterRequest) => onSubmit(data), [
		onSubmit,
	]);

	return (
		<div className="column">
			<h2 className="ui header">Sign Up to Twitch!</h2>
			<form onSubmit={handleSubmit(onFormSubmit)} className="ui large form">
				<div className="ui stacked segment">
					<IconInputField
						icon="envelope"
						toLeft={true}
						inputName="email"
						placeholder="Enter email..."
						refCallback={register({
							required: 'Il campo email è obbligatorio',
							pattern: {
								value: /^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}$/i,
								message: 'Invalid email'
							}
						})}
					/>
					<LabelError 
						text={errors.email && errors.email.message}
					/>
					<IconInputField
						icon="user"
						toLeft={true}
						inputName="username"
						placeholder="Enter username..."
						refCallback={register({
							required: 'Il campo username è obbligatorio',
							maxLength: 12,
							minLength: 4
						})}
					/>
					<LabelError 
						text={errors.username && errors.username.message}
					/>
					<LabelError 
						text={errors.username && errors.username.type === 'maxLength' && 'Max length 12 characters'}
					/>
					<LabelError 
						text={errors.username && errors.username.type === 'minLength' && 'Min length 4 characters'}
					/>
					<IconInputField
						icon="lock"
						type="password"
						inputName="password"
						toLeft={true}
						placeholder="Enter password..."
						refCallback={register({
							required: 'Il campo password è obbligatorio'
						})}
					/>
					<LabelError 
						text={errors.password && errors.password.message}
					/> 
					<IconInputField
						icon="lock"
						type="password"
						inputName="confirmPassword"
						toLeft={true}
						placeholder="Enter confirm password..."
						refCallback={register({
							required: 'Il campo conferma password è obbligatorio'
						})}
					/>
					<LabelError 
						text={errors.confirmPassword && errors.confirmPassword.message}
					/> 
					<BaseInputField
						name="firstName"
						placeholder="Enter name..."
						refCallback={register}
					/>
					<BaseInputField
						name="secondName"
						placeholder="Enter surname..."
						refCallback={register}
					/>
					<BaseButton
						color="blue"
						type="submit"
						text="Register"
						isFullWidth={true}
					/>
				</div>
			</form>
		</div>
	);
};

export default memo(RegistrationForm);
