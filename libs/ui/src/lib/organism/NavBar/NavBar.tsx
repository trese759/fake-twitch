import React, { memo, useCallback } from 'react';
import { BaseLink } from '../../atoms';
import { IconButton } from '../../molecules';
import { useHistory } from 'react-router-dom';

import { useUser, useLogout } from '@fake-twitch/store';

/**
 * @brief Application NavBar component
 * @param onSignIn Function to execute when Sign In button is clicked
 * @param onSignUp Function to execute when Sign Up button is clicked
 */
const NavBar: React.FC = (): JSX.Element => {
	const history = useHistory();
	const { user } = useUser();
	const isSigned = sessionStorage.getItem('token');
	const { logout } = useLogout();

	const signIn = useCallback(() => history.push('/account'), [history]);
	const signUp = useCallback(() => history.push('/account/register'), [
		history,
	]);
	const signOut = useCallback(() => {
		logout();
		history.push('/');
	}, [history, logout]);

	const renderStreamsMenu = () => {
		return (
			<div className="menu">
				<BaseLink to="/streams/my-streams" text="My Streams" />
				<BaseLink to="/streams/new" text="Create Stream" />
			</div>
		);
	};

	const renderSignOut = () => {
		return (
			<div className="right menu">
				<BaseLink to="/" text={user.username} />
				<IconButton
					type="button"
					icon="sign-out"
					color="red"
					text="Sign Out"
					onClick={signOut}
				/>
			</div>
		);
	};

	const renderSignInAndSignUp = () => {
		return (
			<div className="right menu">
				<IconButton
					type="button"
					icon="sign-in"
					color="blue"
					text="Sign In"
					onClick={signIn}
				/>
				<IconButton
					type="button"
					icon="user plus"
					color="blue"
					text="Sign Up"
					onClick={signUp}
				/>
			</div>
		);
	};

	const renderButtons = isSigned ? renderSignOut : renderSignInAndSignUp;
	const renderStreams = isSigned ? renderStreamsMenu : () => null;

	return (
		<div className="ui secondary pointing menu">
			<BaseLink to="/" text="Fake Twitch" />
			<BaseLink to="/" text="Home" />
			{renderStreams()}
			{renderButtons()}
		</div>
	);
};

export default memo(NavBar);
