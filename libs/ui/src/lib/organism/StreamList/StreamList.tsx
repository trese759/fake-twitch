import React, { memo } from 'react';
import { StreamInState } from '@fake-twitch/store';
import { StreamItem } from '../../molecules';

interface Props {
    streams: StreamInState[];
}

/**
 * @brief Render all streams
 * @param streams Array of all streams 
 */
const StreamList: React.FC<Props> = ({streams}): JSX.Element => {
	const streamList = streams.map(stream => <StreamItem stream={stream} key={stream.id} />);
    
	return (
		<div className="ui celled list">
			{streamList}
		</div>
	);
};

export default memo(StreamList);