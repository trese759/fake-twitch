import React, { useCallback, memo } from 'react';
import { useForm } from 'react-hook-form';
import { LabeledInputField } from '../../molecules';
import { BaseButton } from '../../atoms';

import { StreamCreateRequest } from '@fake-twitch/interfaces';
import { StreamInState } from '@fake-twitch/store';
import { useHistory } from 'react-router-dom';

interface Props {
    onSubmit: (data: StreamCreateRequest) => void;
    stream?: StreamInState;
}

/**
 * @brief Form for handle stream editing
 * @param onSubmit Handle form submit
 */
const EditStreamForm: React.FC<Props> = ({ onSubmit, stream }): JSX.Element => {
	const history = useHistory();
	const { register, handleSubmit } = useForm({
		mode: 'onChange',
		defaultValues: {
			title: stream?.title,
			description: stream?.description
		}
	});
    
	const onFormSubmit = useCallback((data: StreamCreateRequest) => {
		data.userId = sessionStorage.getItem('userId');
		data.id = stream?.id;
		onSubmit(data);
	}, [onSubmit, stream]);

	return (
		<div className="column">
			<h2 className="ui header">Editing Stream {stream?.id}</h2>
			<form onSubmit={handleSubmit(onFormSubmit)} className="ui large form">
				<LabeledInputField
					inputName="title"
					labelText="Stream title *"
					placeholder="Insert Stream title..."
					refCallback={register({
						required: 'Stream title is required'
					})}
				/>
				<LabeledInputField
					inputName="description"
					labelText="Stream description *"
					placeholder="Insert Stream description..."
					refCallback={register({
						required: 'Stream description is required'
					})}
				/>
				<BaseButton
					color="green"
					type="submit"
					text="Save Stream Changes"
					isFullWidth={true}
				/>
			</form>
			<div style={{marginTop: 10, textAlign: 'left'}}>
				<BaseButton
					color="blue"
					type="button"
					text="Back to Streams"
					onClick={() => history.push('/streams/my-streams')}
				/>
			</div>
		</div>
	);
};

export default memo(EditStreamForm);
