import React, { memo } from 'react';
import { StreamInState } from '@fake-twitch/store';
import { VideoPlayer } from '../../molecules';

interface Props {
    stream: StreamInState;
};

const VideoStreaming: React.FC<Props> = ({stream}): JSX.Element => {
	return (
		<div>
			<VideoPlayer streamId={stream?.id} />
			<h1>{stream?.title}</h1>
			<h5>{stream?.description}</h5>
		</div>
	);
};

export default memo(VideoStreaming);