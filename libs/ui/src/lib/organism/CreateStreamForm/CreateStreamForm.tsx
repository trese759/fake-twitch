import React, { useCallback, memo } from 'react';
import { useForm } from 'react-hook-form';
import { LabeledInputField } from '../../molecules';
import { BaseButton, LabelError } from '../../atoms';

import { StreamCreateRequest } from '@fake-twitch/interfaces';

interface Props {
	onSubmit: (data: StreamCreateRequest) => void;
}

/**
 * @brief Form for handle stream creation
 * @param onSubmit Handle form submit
 */
const CreateStreamForm: React.FC<Props> = ({ onSubmit }): JSX.Element => {
	const { register, handleSubmit, errors } = useForm({
		mode: 'onChange'
	});
	const onFormSubmit = useCallback((data: StreamCreateRequest) => {
		data.userId = sessionStorage.getItem('userId');
		onSubmit(data);
	}, [onSubmit]);

	return (
		<div className="column">
			<h2 className="ui header">Create a Stream</h2>
			<form onSubmit={handleSubmit(onFormSubmit)} className="ui large form">
				<LabeledInputField
					inputName="title"
					labelText="Stream title *"
					placeholder="Insert Stream title..."
					refCallback={register({
						required: 'Stream title is required'
					})}
				/>
				<LabelError 
					text={errors.title && errors.title.message}
				/>
				<LabeledInputField
					inputName="description"
					labelText="Stream description *"
					placeholder="Insert Stream description..."
					refCallback={register({
						required: 'Stream description is required'
					})}
				/>
				<LabelError 
					text={errors.description && errors.title.description}
				/>
				<BaseButton
					color="purple"
					type="submit"
					text="Create Stream"
					isFullWidth={true}
				/>
			</form>
		</div>
	);
};

export default memo(CreateStreamForm);
