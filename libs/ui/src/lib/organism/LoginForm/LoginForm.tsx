import React, { memo, useCallback } from 'react';
import { useForm } from 'react-hook-form';

import { IconInputField } from '../../molecules';
import { BaseButton, LabelError } from '../../atoms';
import { LoginRequest } from '@fake-twitch/interfaces';

interface Props {
	onSubmit: (data: LoginRequest) => void;
}

const LoginForm: React.FC<Props> = ({ onSubmit }): JSX.Element => {
	const { register, handleSubmit, errors } = useForm();
	const onFormSubmit = useCallback((data: LoginRequest) => {
		onSubmit(data);
	}, [onSubmit]);

	return (
		<div className="column">
			<h2 className="ui header">Sign In to Twitch!</h2>
			<form onSubmit={handleSubmit(onFormSubmit)} className="ui large form">
				<div className="ui stacked segment">
					<IconInputField
						icon="user"
						toLeft={true}
						inputName="identifier"
						placeholder="Enter username..."
						refCallback={register({
							required: 'Username obbligatorio'
						})}
					/>
					<LabelError 
						text={errors.identifier && errors.identifier.message}
					/> 
					<IconInputField
						icon="lock"
						type="password"
						inputName="password"
						toLeft={true}
						placeholder="Enter password..."
						refCallback={register({
							required: 'Password obbligatoria'
						})}
					/>
					<LabelError 
						text={errors.password && errors.password.message}
					/> 
					<BaseButton
						color="blue"
						type="submit"
						text="Log In"
						isFullWidth={true}
					/>
				</div>
			</form>
		</div>
	);
};

export default memo(LoginForm);
