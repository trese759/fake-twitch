import React, { useCallback, memo } from 'react';
import { BaseInputField } from '../../atoms';
import { FieldElement } from 'react-hook-form';

interface Props {
	inputName: string;
	toLeft: boolean;
	icon: string;
	type?: string;
	inputState?: string;
	placeholder?: string;
	onInput?: () => void;
	refCallback?: (ref: FieldElement) => void;
}

/**
 * @brief Iconed Input Field element
 * @param type Type of input (text, password, number, etc.)
 * @default type 'text'
 * @param name Name of input
 * @param inputState State of input (error, warning, etc.)
 * @param toLeft If true then children are renderer at left else at right (inner)
 * @param icon Icon to render
 * @param onInput Function called when user press a key on input field focused
 */
const IconInputField: React.FC<Props> = ({
	inputName,
	icon,
	toLeft,
	refCallback,
	onInput,
	type,
	inputState,
	placeholder,
}): JSX.Element => {
	const onInputChange = useCallback(() => onInput, [onInput]);

	return (
		<BaseInputField
			isLabeled={false}
			name={inputName}
			hasLeftChild={toLeft}
			hasRightChild={!toLeft}
			onInput={onInputChange}
			placeholder={placeholder}
			type={type}
			inputState={inputState}
			refCallback={refCallback}
		>
			<i className={`${icon} icon`} />
		</BaseInputField>
	);
};

export default memo(IconInputField);
