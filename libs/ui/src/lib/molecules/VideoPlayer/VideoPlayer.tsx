import React, { memo, useEffect } from 'react';
import flv from 'flv.js';

interface Props {
    streamId: number;
}

const VideoPlayer: React.FC<Props> = ({streamId}): JSX.Element => {
	const vRef: React.RefObject<HTMLVideoElement> = React.createRef();

	useEffect(() => {
		if (streamId) {
			const flvPlayer = flv.createPlayer({
				type: 'flv',
				url: `http://localhost:8000/live/${streamId}.flv`
			});
            
			flvPlayer.attachMediaElement(vRef.current);
			flvPlayer.load();
		}
	}, [streamId, vRef]);

	return (
		<video 
			ref={vRef}
			controls
			style={{width: '100%'}}
		/>
	);
};

export default memo(VideoPlayer);