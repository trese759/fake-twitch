import React, { useCallback, memo } from 'react';
import { BaseInputField } from '../../atoms';
import { FieldElement } from 'react-hook-form';

interface Props {
	inputName: string;
	labelText: string;
	type?: string;
	inputState?: string;
	placeholder?: string;
	initialValue?: string;
	onInput?: () => void;
	refCallback?: (ref: FieldElement) => void;
}

/**
 * @brief Iconed Input Field element
 * @param type Type of input (text, password, number, etc.)
 * @default type 'text'
 * @param initialValue Initial value of input
 * @param name Name of input
 * @param inputState State of input (error, warning, etc.)
 * @param labelText Text of label
 * @param onInput Function called when user press a key on input field focused
 */
const LabeledInputField: React.FC<Props> = ({
	inputName,
	refCallback,
	onInput,
	type,
	labelText,
	inputState,
	initialValue,
	placeholder,
}): JSX.Element => {
	const onInputChange = useCallback(() => {
		try {
			return onInput();
		} catch {
			return onInput;
		}
	}, [onInput]);

	return (
		<BaseInputField
			isLabeled={true}
			name={inputName}
			onInput={onInputChange}
			placeholder={placeholder}
			type={type}
			labelText={labelText}
			inputState={inputState}
			refCallback={refCallback}
		/>
	);
};

export default memo(LabeledInputField);
