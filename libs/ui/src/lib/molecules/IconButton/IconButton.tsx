import React, { memo } from 'react';
import { BaseButton } from '../../atoms';

interface Props {
	color: string;
	icon: string;
	type: 'button' | 'submit' | 'reset';
	isFullWidth?: boolean;
	text?: string;
	onClick?: () => void;
}

/**
 * @brief Base Button element
 * @param color Semantic UI color class
 * @param isFullWidth If true the button fill container width
 * @param type Type of button
 * @param text Button text
 * @param onClick Function to execute when button is clicked
 */
const IconButton: React.FC<Props> = ({
	icon,
	isFullWidth,
	type,
	color,
	text,
	onClick,
}): JSX.Element => {
	return (
		<BaseButton
			color={color}
			text={text ? text : ''}
			onClick={onClick}
			type={type}
			isFullWidth={isFullWidth}
		>
			<i className={`${icon} icon`} />
		</BaseButton>
	);
};

export default memo(IconButton);
