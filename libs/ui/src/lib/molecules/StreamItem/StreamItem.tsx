import React, { memo, useMemo } from 'react';
import { StreamInState } from '@fake-twitch/store';
import { IconButton } from '../IconButton';
import { useHistory } from 'react-router-dom';

interface Props {
    stream: StreamInState;
}

/**
 * @brief Render item for Stream
 * @param stream Stream informations 
 */
const StreamItem: React.FC<Props> = ({stream}): JSX.Element => {
	const history = useHistory();
	const isCreatedByCurrentUser = sessionStorage.getItem('userId') === stream.userId?.toString();

	const adminButtons = useMemo(() => {
		return (
			<div className="right floated content">
				<IconButton
					text="Edit"
					color="green"
					icon="edit"
					type="button"
					onClick={() => history.push(`/streams/edit/${stream.id}`)}
				/>
				<IconButton
					text="Delete"
					color="red"
					icon="trash"
					type="button"
					onClick={() => history.push(`/streams/delete/${stream.id}`)}
				/>
			</div>
		);
	}, [history, stream.id]);

	return (
		<div className="item">
			{isCreatedByCurrentUser ? adminButtons : null}
			<i className="large middle aligned icon camera" />
			<div 
				className="content"
				style={{cursor: 'pointer'}}
				onClick={() => history.push(`/stream/${stream.id}`)}
			>
				<strong>{stream.title}</strong>
				<div className="description">{stream.description}</div>
			</div>
		</div>
	);
};

export default memo(StreamItem);