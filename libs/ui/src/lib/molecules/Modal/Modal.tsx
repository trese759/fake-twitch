import React, { memo } from 'react';
import ReactDOM from 'react-dom';

interface Props {
    header: string;
    content: string;
    actions: JSX.Element;
    onDismiss: () => void;
}

const Modal: React.FC<Props> = ({header, content, actions, onDismiss}): JSX.Element => {
	return ReactDOM.createPortal(
		<div 
			className="ui dimmer modals visible active"
			onClick={onDismiss}
		>
			<div 
				className="ui standard modal visible active"
				onClick={(e) => e.stopPropagation()}
			>
				<div className="header">{header}</div>
				<div className="content">{content}</div>
				<div className="actions">{actions}</div>
			</div>
		</div>,
		document.querySelector('#modal')
	);
};

export default memo(Modal);