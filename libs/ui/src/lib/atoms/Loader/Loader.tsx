import React, { memo } from 'react';

export interface Props {
	size: 'small' | 'medium' | 'large';
	isCentered: boolean;
}

const Loader: React.FC<Props> = ({ size, isCentered }): JSX.Element => {
	return (
		<div
			className={`ui active ${
				isCentered ? 'centered' : ''
			} ${size} inline loader`}
		/>
	);
};

export default memo(Loader);
