import React, { memo } from 'react';
import { Link } from 'react-router-dom';

interface Props {
	to: string;
	text: string;
}

/**
 * @brief Base link element
 * @param to Path to load when link is clicked (href)
 * @param text Link text to show
 */
const BaseLink: React.FC<Props> = ({ to, text }): JSX.Element => {
	return (
		<Link to={to} className="item">
			<strong>{text}</strong>
		</Link>
	);
};

export default memo(BaseLink);
