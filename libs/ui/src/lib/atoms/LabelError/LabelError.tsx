import React, { memo, CSSProperties } from 'react';

interface Props {
    text: string;
}

const LabelError: React.FC<Props> = ({text}): JSX.Element => {
	return (
		<label style={labelStyle}>
			{text}
		</label>
	);
};

const labelStyle: CSSProperties = {
	color: 'red', 
	float: 'left', 
	marginTop: -15, 
	marginBottom: 10,
	fontSize: 12
};

export default memo(LabelError);