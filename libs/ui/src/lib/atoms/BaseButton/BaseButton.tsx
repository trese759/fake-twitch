import React, { useCallback, memo } from 'react';

interface Props {
	color: string;
	type?: 'button' | 'submit' | 'reset';
	text?: string;
	isFullWidth?: boolean;
	children?: JSX.Element;
	onClick?: () => void;
}

/**
 * @brief Base Button element
 * @param color Semantic UI color class
 * @param isFullWidth If true the button fill container width
 * @param type Type of button
 * @param text Button text
 * @param onClick Function to execute when button is clicked
 */
const BaseButton: React.FC<Props> = ({
	color,
	isFullWidth,
	type,
	text,
	onClick,
	children,
}): JSX.Element => {
	const onButtonClick = useCallback(() => {
		try {
			return onClick();
		} catch {
			return onClick;
		}
	}, [onClick]);

	return (
		<button
			className={`${isFullWidth ? 'fluid' : null} ui button ${color}`}
			onClick={onButtonClick}
			type={type}
		>
			{children}
			{text}
		</button>
	);
};

export default memo(BaseButton);
