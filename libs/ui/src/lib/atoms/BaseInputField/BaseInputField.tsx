import React, { useCallback, memo } from 'react';
import { FieldElement } from 'react-hook-form';

interface Props {
	type?: string;
	isLabeled?: boolean;
	labelText?: string;
	name: string;
	hasLeftChild?: boolean;
	hasRightChild?: boolean;
	inputState?: string;
	placeholder?: string;
	children?: JSX.Element;
	initialValue?: string;
	onInput?: () => void;
	refCallback?: (ref: FieldElement) => void;
}

/**
 * @brief Input Field element
 * @param type Type of input (text, password, number, etc.)
 * @default type 'text'
 * @param name Name of input
 * @param isLabeled If true add a label to input
 * @param initialValue Initial value of input
 * @param labelText If isLabeled show
 * @param inputState State of input (error)
 * @param hasLeftChild If true then children are renderer at left (inner)
 * @param hasRightChild If true then children are rendered at right (inner)
 * @param children Input field children (thinked for icons)
 * @param onInput Function called when user press a key on input field focused
 */
const BaseInputField: React.FC<Props> = ({
	type,
	name,
	placeholder,
	inputState,
	isLabeled,
	labelText,
	refCallback,
	hasLeftChild,
	hasRightChild,
	initialValue,
	children,
	onInput,
}): JSX.Element => {
	const onInputChange = useCallback(() => {
		try {
			return onInput();
		} catch {
			return onInput;
		}
	}, [onInput]);
	const iconClass = hasLeftChild || hasRightChild ? 'icon' : null;
	const leftChild = hasLeftChild ? 'left' : null;

	return (
		<div className="field">
			{isLabeled ? (
				<label style={{ textAlign: 'left' }}>{labelText}</label>
			) : null}
			<div className={`ui ${leftChild + ' ' + iconClass} input ${inputState}`}>
				{hasLeftChild ? children : null}
				<input
					type={type ? type : 'text'}
					name={name}
					placeholder={placeholder}
					onChange={onInputChange}
					ref={refCallback}
					defaultValue={initialValue || ''}
				/>
				{hasRightChild ? children : null}
			</div>
		</div>
	);
};

export default memo(BaseInputField);
