import React from 'react';
import { Provider } from 'react-redux';
import ReactDOM from 'react-dom';

import App from './app/app';
import { storeConfig, initialAppState } from '@fake-twitch/store';

ReactDOM.render(
	<Provider store={storeConfig(initialAppState)}>
		<App />
	</Provider>,
	document.getElementById('root')
);
