import React, { memo, useCallback, useState, useEffect } from 'react';
import { useHistory } from 'react-router-dom';

import { LoginForm, Loader } from '@fake-twitch/ui';
import { LoginRequest } from '@fake-twitch/interfaces';
import { useLogin } from '@fake-twitch/store';

/** @brief SignIn page for login */
const SignIn: React.FC = (): JSX.Element => {
	const history = useHistory();
	const [loginRequest, setLoginRequest] = useState(null);
	const [isLoginClicked, setIsLoginClicked] = useState(false);
	const { login, isFetched, error } = useLogin(loginRequest);

	const onFormSubmit = useCallback((data: LoginRequest) => {
		setIsLoginClicked(true);
		setLoginRequest(data);
	}, []);

	useEffect(() => {
		if (loginRequest && !isFetched) {
			login();
		} else if (isFetched) {
			setIsLoginClicked(false);
			if (!error) {
				history.push('/streams/my-streams');
			}
		}
	}, [loginRequest, login, isFetched, history, error]);

	const renderLoader = () => <Loader size="large" isCentered={true} />;
	const renderForm = () => {
		return (
			<div>
				<div className="ui middle aligned center aligned grid">
					<LoginForm onSubmit={(data) => onFormSubmit(data)} />
				</div>
				<div style={{ color: 'red' }}>
					{error ? JSON.parse(error).message[0].messages[0].message : null}
				</div>
			</div>
		);
	};

	if (!isFetched && isLoginClicked) {
		return renderLoader();
	} else {
		return renderForm();
	}
};

export default memo(SignIn);
