import React, { memo, useCallback, useState, useEffect, useMemo } from 'react';
import { useHistory } from 'react-router-dom';

import { RegistrationForm, Loader } from '@fake-twitch/ui';
import { RegisterRequest } from '@fake-twitch/interfaces';
import { useRegister } from '@fake-twitch/store';

/** @brief SignUp page for login */
const SignUp: React.FC = (): JSX.Element => {
	const history = useHistory();
	const [registerRequest, setRegisterRequest] = useState(null);
	const [isRegisterClicked, setIsRegisterClicked] = useState(false);
	const { register, isFetched, error } = useRegister(registerRequest);

	const onFormSubmit = useCallback((data: RegisterRequest) => {
		setIsRegisterClicked(true);
		setRegisterRequest(data);
	}, []);

	useEffect(() => {
		if (registerRequest && !isFetched) {
			register();
		} else if (isFetched) {
			setIsRegisterClicked(false);
			if (error) {
				history.push('/streams/my-streams');
			}
		}
	}, [registerRequest, register, isFetched, history, error]);

	const loader = useMemo(() => <Loader size="large" isCentered={true} />, []);
	const form = useMemo(() => {
		return (
			<div>
				<div className="ui middle aligned center aligned grid">
					<RegistrationForm
						onSubmit={(data: RegisterRequest) => onFormSubmit(data)}
					/>
				</div>
				<div style={{ color: 'red' }}>
					{error ? JSON.parse(error).message[0].messages[0].message : null}
				</div>
			</div>
		);
	}, [error, onFormSubmit]);

	if (!isFetched && isRegisterClicked) {
		return loader;
	} else {
		return form;
	}
};

export default memo(SignUp);
