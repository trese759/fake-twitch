import React, { memo, useCallback, useState, useEffect, useMemo } from 'react';

import { CreateStreamForm, Loader } from '@fake-twitch/ui';
import { StreamCreateRequest } from '@fake-twitch/interfaces';
import { useStreams } from '@fake-twitch/store';
import { useHistory } from 'react-router-dom';

/** @brief Page for create new stream */
const StreamCreate: React.FC = (): JSX.Element => {
	const history = useHistory();
	const [isLoading, setIsLoading] = useState(false);
	const [lastStreamLength, setLastStreamLength] = useState(0);
	const { createStream, streams } = useStreams();

	const currentStreamLength = Object.keys(streams).length;
	const isLastStreamLengthEqual = lastStreamLength === currentStreamLength;

	const onFormSubmit = useCallback((data: StreamCreateRequest) => {
		setIsLoading(true);
		createStream(data);
	}, [createStream]);

	useEffect(() => {
		setLastStreamLength(currentStreamLength);
		if (isLoading && isLastStreamLengthEqual) {
			history.push('/streams/my-streams');
		}
	}, [currentStreamLength, history, isLastStreamLengthEqual, isLoading]);

	const loader = useMemo(() => <Loader size="large" isCentered={true} />, []);
	const form = useMemo(() => {
		return (
			<div>
				<div className="ui middle aligned center aligned grid">
					<CreateStreamForm onSubmit={onFormSubmit} />
				</div>
				<div style={{ color: 'green' }}>
					{null}
				</div>
			</div>
		);
	}, [onFormSubmit]);

	if (isLoading && !isLastStreamLengthEqual) {
		return loader;
	} else {
		return form;
	}
};

export default memo(StreamCreate);
