/* eslint-disable @typescript-eslint/no-explicit-any */
import React, { memo, useMemo, useCallback, useState } from 'react';
import { EditStreamForm, Loader } from '@fake-twitch/ui';
import { useStreams } from '@fake-twitch/store';
import { useHistory } from 'react-router-dom';
import { StreamCreateRequest } from '@fake-twitch/interfaces';

interface Props {
	match: any;
}

/** @brief Page for edit specific stream */
const StreamEdit: React.FC<Props> = ({match}): JSX.Element => {
	const history = useHistory();
	const [isLoading, setIsLoading] = useState(false);
	const { editStream, streams } = useStreams();
	const editingStream = streams[match.params['id']];
	const isStreamOfCurrentUser = editingStream?.userId.toString() === sessionStorage.getItem('userId');

	const onFormSubmit = useCallback((data: StreamCreateRequest) => {
		setIsLoading(true);
		editStream(data);
		history.push('/streams/my-streams');
	}, [editStream, history]);

	const errorMessage = useMemo(() => {
		return <h1 style={{color: 'red'}}>Cannot edit Stream of other users!</h1>
	}, []);
	const loader = useMemo(() => <Loader size="large" isCentered={true} />, []);
	const form = useMemo(() => {
		return (
			<div>
				<div className="ui middle aligned center aligned grid">
					<EditStreamForm 
						stream={editingStream}
						onSubmit={onFormSubmit} 
					/>
				</div>
				<div style={{ color: 'green' }}>
					{null}
				</div>
			</div>
		);
	}, [editingStream, onFormSubmit]);

	if (!isStreamOfCurrentUser && editingStream) {
		return errorMessage;
	} else if (editingStream && !isLoading) {
		return form;
	} else {
		return loader;
	}
};

export default memo(StreamEdit);
