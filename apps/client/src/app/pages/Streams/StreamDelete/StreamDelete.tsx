/* eslint-disable @typescript-eslint/no-explicit-any */
import React, { memo, useMemo, useState, useCallback } from 'react';

import { Modal, IconButton, Loader } from '@fake-twitch/ui';
import { useHistory } from 'react-router-dom';
import { useStreams } from '@fake-twitch/store';

interface Props {
	match?: any;
}

/** @brief Page for delete specific stream */
const StreamDelete: React.FC<Props> = ({match}): JSX.Element => {
	const history = useHistory();
	const [isLoading, setIsLoading] = useState(false);
	const { deleteStream, streams } = useStreams();
	const deletingStream = streams[match.params['id']];
	const isStreamOfCurrentUser = deletingStream?.userId.toString() === sessionStorage.getItem('userId');

	const onDelete = useCallback(() => {
		setIsLoading(true);
		deleteStream(deletingStream?.id);
		history.push('/streams/my-streams');
	}, [deleteStream, deletingStream, history]);

	const errorMessage = useMemo(() => {
		return <h1 style={{color: 'red'}}>Cannot delete Stream of other users!</h1>
	}, []);
	
	const loader = useMemo(() => <Loader size="large" isCentered={true} />, []);

	const actions = useMemo(() => {
		return (
			<React.Fragment>
				<IconButton
					color="red"
					icon="trash"
					type="button"
					text="Delete"
					onClick={onDelete}
				/>
				<IconButton
					color="blue"
					icon="times"
					type="button"
					text="Cancel"
					onClick={() => history.goBack()}
				/>
			</React.Fragment>
		);
	}, [history, onDelete]);

	const modal = useMemo(() => {
		return (
			<Modal
				header="Delete Stream"
				content={`Are you sure you want to delete Stream: ${deletingStream?.title}?`}
				actions={actions}
				onDismiss={() => history.goBack()}
			/>
		);
	}, [actions, deletingStream, history]);

	if (!isStreamOfCurrentUser && deletingStream) {
		return errorMessage;
	} else if (deletingStream && !isLoading) {
		return modal;
	} else {
		return loader;
	}
};

export default memo(StreamDelete);
