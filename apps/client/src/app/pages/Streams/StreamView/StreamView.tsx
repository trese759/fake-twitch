/* eslint-disable @typescript-eslint/no-explicit-any */
import React, { memo, useEffect, useState, useMemo } from 'react';
import { useStreams, StreamInState } from '@fake-twitch/store';
import { Loader, VideoStreaming } from '@fake-twitch/ui';

interface Props {
	match: any;
}

/** @brief Page for view specific stream */
const StreamView: React.FC<Props> = ({match}): JSX.Element => {
	const [stream, setStream] = useState({} as StreamInState);
	const { streams } = useStreams();

	useEffect(() => {
		setStream(streams[match.params.id]);
	}, [streams, match.params.id])

	const loader = useMemo(() => <Loader isCentered={true} size="large" />, []);
	const streaming = useMemo(() => {
		return <VideoStreaming stream={stream} />;
	}, [stream]);

	if (!stream) {
		return loader;
	} else {
		return streaming;
	}
};

export default memo(StreamView);
