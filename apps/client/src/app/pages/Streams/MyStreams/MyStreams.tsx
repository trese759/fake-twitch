import React, { memo, useMemo, useCallback } from 'react';
import { useStreams } from '@fake-twitch/store';
import { StreamList, Loader, IconButton } from '@fake-twitch/ui';
import { useHistory } from 'react-router-dom';

/** @brief Page for view specific stream */
const MyStreams: React.FC = (): JSX.Element => {
	const history = useHistory();
	const { streams, isFetched } = useStreams();

	const goToCreateStream = useCallback(() => history.push('/streams/new'), [history]);

	const loader = useMemo(() => <Loader size="large" isCentered={true} />, []);
	const streamList = useMemo(() => {
		const userStreams = Object.values(streams).filter(stream => {
			return stream.userId?.toString() === sessionStorage.getItem('userId');
		});
		return <StreamList streams={userStreams} />;
	}, [streams]);

	return (
		<div>
			<h2>Streams</h2>
			{isFetched ? streamList : loader}
			<IconButton 
				color="purple"
				icon="plus"
				type="button"
				text="Create Stream"
				onClick={goToCreateStream}
			/>
		</div>
	);
};

export default memo(MyStreams);
