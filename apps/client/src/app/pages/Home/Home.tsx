import React, { memo, useMemo } from 'react';
import { useStreams } from '@fake-twitch/store';
import { StreamList, Loader } from '@fake-twitch/ui';

/** @brief Application Home Page contains list of all streams ordered by newest */
const Home: React.FC = (): JSX.Element => {
	const { streams, isFetched } = useStreams();

	const loader = useMemo(() => <Loader size="large" isCentered={true} />, []);
	const streamList = useMemo(() => {
		return <StreamList streams={Object.values(streams)} />;
	}, [streams]);

	return (
		<div>
			<h2>Streams</h2>
			{isFetched ? streamList : loader}
		</div>
	);
};

export default memo(Home);
