import React from 'react';
import { Router, Route, Switch } from 'react-router-dom';
import { createBrowserHistory } from 'history';

import { NavBar } from '@fake-twitch/ui';
import {
	Home,
	StreamCreate,
	StreamEdit,
	StreamDelete,
	StreamView,
	SignIn,
	SignUp,
	MyStreams,
} from './pages';

/** @brief Application routing definitions */
const AppRouting: React.FC = (): JSX.Element => {
	return (
		<Router history={createBrowserHistory()}>
			<NavBar />
			<div className="ui container">
				<Switch>
					<Route path="/" exact component={Home} />
					<Route path="/account" exact component={SignIn} />
					<Route path="/account/register" component={SignUp} />
					<Route path="/stream/:id" exact component={StreamView} />
					<Route path="/streams/my-streams" exact component={MyStreams} />
					<Route path="/streams/new" exact component={StreamCreate} />
					<Route path="/streams/edit/:id" exact component={StreamEdit} />
					<Route path="/streams/delete/:id" exact component={StreamDelete} />
				</Switch>
			</div>
		</Router>
	);
};

export default AppRouting;
