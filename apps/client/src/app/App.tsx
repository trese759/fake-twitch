import React, { memo } from 'react';

import AppRouting from './AppRouting';

/** @brief Application Entry Point */
const App: React.FC = (): JSX.Element => {
	return (
		<div>
			<AppRouting />
		</div>
	);
};

export default memo(App);
